---
layout: page
title: For Users
konqi: /assets/img/konqi-docbook.png
---

## New to Kontact?

Kontact offers countless features and options. To find out about them you can read some of the handbooks:

* [Kontact Handbook](https://docs.kde.org/stable5/en/kontact/kontact/index.html)
* [KMail Handbook](https://docs.kde.org/stable5/en/kmail/kmail2/index.html)
* [KOrganizer Handbook](https://docs.kde.org/stable5/en/korganizer/korganizer/index.html)
* [Akregator Handbook](https://docs.kde.org/stable5/en/akregator/akregator/index.html)
* [More handbooks, also in other languages...](https://docs.kde.org/index.php?language=en&amp;package=pim)

## Need Help?

Check the Kontact &amp; PIM subforum on [KDE Forums](https://forum.kde.org/viewforum.php?f=215),
maybe someone has already solved a similar problem. And if not, feel free to ask
there!

You can also ask on [#kontact IRC channel](irc://freenode.net/#kontact) on Freenode.
If you prefer email communication, you can ask on the [kdepim-users](https://mail.kde.org/mailman/listinfo/kdepim-users)
mailing list.

## Found a Bug?

Bugs in software happen. If you found some in Kontact, please report them into
[our Bugzilla](https://bugs.kde.org). Please do not report bugs on forums or
mailinglists - such reports will most likely be overlooked or forgotten by the
developers quite soon. To make sure the bug gets noticed by developers and
fixed as soon as possible, report it into Bugzilla.

## Missing a Feature?

Kontact has a lot of features, but there's always a room for one or two more.
If you have an idea for an improvement or you are missing a feature in Kontact,
write your suggestion in our [Bugzilla](https://bugs.kde.org) so that the
developers can find it and implement it.

